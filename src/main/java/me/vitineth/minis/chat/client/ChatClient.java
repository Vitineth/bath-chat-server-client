package me.vitineth.minis.chat.client;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 * File created by Ryan (vitineth).<br>
 * Created on 17/02/2018.
 *
 * @author Ryan (vitineth)
 * @since 17/02/2018
 */
public class ChatClient {

    public static void main(String[] args) {
        // This is the default port that the specification requires.
        int port = 14001;
        // This is the default host that the specification requires.
        String host = "localhost";

        // If the user has provided actual arguments to the program we will need to check them all in case they have
        // provided a different port or address for the client to connect to
        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                // If they have provided the port argument, we need to check that there is another parameter after
                // it so that we can actually parse it and change the port. If not tell them they are stupid and
                // exit
                if (args[i].equals("-cca")) {
                    if (i == args.length - 1) {
                        System.err.println("You must specify a valid address after the -cca flag");
                        return;
                    } else {
                        // If they have missed out the actual address and then provided a port, we will let them know.
                        // This is not required but I thought I would throw it in
                        if (args[i + 1].equalsIgnoreCase("-ccp")) {
                            System.err.println("You must provide a valid address after the -cca flag");
                        } else {
                            // Otherwise we just get the address they want to connect to and set it.
                            host = args[i + 1];
                        }
                    }
                }
                // If they have provided the port argument, we need to check that there is another parameter after
                // it so that we can actually parse it and change the port. If not tell them they are stupid and
                // exit
                if (args[i].equals("-ccp")) {
                    if (i == args.length - 1) {
                        System.err.println("You must specify a valid port after the -ccp flag");
                        return;
                    } else {
                        // As my computing teachers tend to say "users are idiots, if you ask them what 2 + 2 in,
                        // one user will give you '2', another will give you 'two' and one will just lick the keyboard"
                        // So as a result, we need to try casting the input they gave (which is a string) to an integer
                        // this will fail if it is anything but a set of numbers, however the function throws a runtime
                        // error which means you technically don't NEED to catch it but it will screw you if you don't.
                        try {
                            port = Integer.parseInt(args[i + 1]);
                            if (port < 1024) {
                                System.out.println("Running with this port may require admin privileges.");
                            }
                        } catch (NumberFormatException e) {
                            System.err.println("You must provide a valid numerical port for the server after the -ccp flag");
                            return;
                        }
                    }
                }
            }
        }

        // Then we try to connect to the server. If something goes wrong, we output the error and exit.
        Socket socket;
        try {
            socket = new Socket(host, port);
        } catch (IOException e) {
            System.err.println("Failed to connect to the server, stack trace to follow");
            e.printStackTrace();
            return;
        }

        // Then we create the runnable which wraps all of the reading code
        ReadRunnable readRunnable = new ReadRunnable(socket);

        // Then we create and start the thread in which this code will execute. This means that messages can be read
        // at the same time as sent.
        Thread readThread = new Thread(readRunnable, "Read Runnable");
        readThread.start();

        // Then, like the server, we create a scanner to read from the client.
        Scanner scanner = new Scanner(System.in);
        String line;

        boolean quitingDueToError = false;

        // While the client has not told us to exit, read the input into line so we can actually use it in the loop
        while (!(line = scanner.nextLine()).equalsIgnoreCase("exit")) {
            try {
                // Get the input's length and append it to the front with a colon so that the server knows how much to
                // read and then send it to the server
                socket.getOutputStream().write((line.length() + ":" + line).getBytes());
            } catch (IOException e) {
                if (e.getMessage().endsWith("socket write error")){
                    quitingDueToError = true;
                    System.err.println("Server has disconnected. Exiting");
                    break;
                }

                System.err.println("Failed to write to the server, stack trace to follow");
                e.printStackTrace();
            }
        }

        // If we have got to this point because we couldn't write to the server, then we don't want to try and send
        // another message to them. Therefore we just skip this bit and move on to shutting down the client.
        if (!quitingDueToError) {
            // If we get to this point, it means that the client has typed 'exit' in order to quit. Therefore we attempt
            // to tell the server that we want to quit and that the client will be disconnecting in a few seconds.
            try {
                socket.getOutputStream().write("4:exit".getBytes());
            } catch (IOException e) {
                System.err.println("Failed to write to the server, stack trace to follow");
                e.printStackTrace();
            }
        }

        // Then we stop accepting inputs from the command line, then we attempt to stop reading from the server and
        // end the thread completely.
        try {
            scanner.close();
            readRunnable.stop();
            readThread.join();
        } catch (InterruptedException e) {
            System.err.println("Failed to end the read thread");
            e.printStackTrace();
        }

        // Finally we close the socket that we have been communicating on and that will officially close our connection
        // to the server. At this point everything is finished all clean so we can just exit.
        try {
            socket.close();
        } catch (IOException e) {
            System.err.println("Failed to close the server, stack trace to follow");
            e.printStackTrace();
        }
    }

}
