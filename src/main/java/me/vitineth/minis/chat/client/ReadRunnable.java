package me.vitineth.minis.chat.client;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * File created by Ryan (vitineth).<br>
 * Created on 17/02/2018.
 *
 * @author Ryan (vitineth)
 * @since 17/02/2018
 */
public class ReadRunnable implements Runnable{

    private final Socket socket;
    private boolean running;

    public ReadRunnable(Socket socket) {
        this.socket = socket;
        this.running = true;
    }

    public void stop(){
        running = false;
    }

    @Override
    public void run() {

        // SEE 'MessageRunnable' in the SERVER.
        while (running){
            synchronized (socket){
                try {
                    if (socket.getInputStream().available() > 0){
                        InputStream is = socket.getInputStream();

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        int read;
                        while ((read = is.read()) != -1){
                            if ((char) read == ':') break;
                            baos.write(read);
                        }

                        byte[] buffer = new byte[Integer.parseInt(baos.toString(StandardCharsets.UTF_8.name()))];
                        is.read(buffer);

                        String message = new String(buffer, StandardCharsets.UTF_8);

                        // Because we are not sending this to anyone, we can just print it to the client.
                        System.out.println(message);
                    }
                } catch (IOException e) {
                    System.err.println("Failed to read from the client, stack trace to follow");
                    e.printStackTrace();
                }
            }
        }
    }

}
