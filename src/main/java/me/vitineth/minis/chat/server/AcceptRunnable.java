package me.vitineth.minis.chat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * File created by Ryan (vitineth).<br>
 * Created on 17/02/2018.
 *
 * @author Ryan (vitineth)
 * @since 17/02/2018
 */
public class AcceptRunnable implements Runnable {

    /**
     * A reference to the server socket on which the server is acting
     */
    private ServerSocket socket;
    /**
     * Whether the current runnable is considered to be running
     */
    private boolean running;

    public AcceptRunnable(ServerSocket socket) {
        this.socket = socket;
        this.running = true;
    }

    /**
     * Attempts to stop the runnable.<br>
     * <b>Warning</b>: This does not actually terminate the server, if the loop is executing then it may drop out
     * but if accept is blocking, this will not terminate the runnable
     */
    public void stop() {
        running = false;
    }

    @Override
    public void run() {
        // We need to keep running while this thread is considered to be running.
        while (running) {
            try {
                // Then, we try to accept a new client. This will just wait until a new client is received. Accept
                // blocks until a new client is received and as explored in ChatServer, this is only interrupted by
                // closing the socket.
                Socket client = socket.accept();
                // Then we construct a new client to wrap the socket and add it to the server storage. This is sort of
                // point 1 in the server specification but that's covered more in the ServerStorage
                ServerStorage.addClient(new Client(client));
            } catch (IOException e) {
                // If something goes wrong we need to check whether the socket has been closed. If it is, it means that
                // the main thread has closed the socket in an attempt to get this thread to exit so we can ignore the
                // error as it is intentional. Otherwise, something has actually gone really wrong so we need to dump
                // the error.
                if (!socket.isClosed())
                    e.printStackTrace();
            }
        }
    }
}
