package me.vitineth.minis.chat.server;

import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * File created by Ryan (vitineth).<br>
 * Created on 17/02/2018.
 *
 * @author Ryan (vitineth)
 * @since 17/02/2018
 */
public class ServerStorage {

    /**
     * This is a thread-safe list implementation of clients. Basically this means that the underlying data structure
     * is designed to allow multiple threads to interact with it. This isn't actually super required as long as we are
     * careful with synchronizations I think but the specification said 'make sure you use locks or implement
     * concurrent data structures' so I thought I'd threw it in.
     */
    private final static ConcurrentSkipListSet<Client> clients = new ConcurrentSkipListSet<>();

    /**
     * This function adds a client to the list of clients. It is marked as synchronised which is an indicator that it
     * will make sure that this function can only be called one at a time. If if is called twice at exactly the same
     * time, it will make one wait until the other is completed and make sure that its changes are echoed to the other
     * waiting thread
     *
     * @param client {@link Client} The client to add
     * @return boolean <code>true</code> if this set did not already contain the specified element
     */
    public synchronized static boolean addClient(Client client) {
        // Here we used a synchronized block. By using this we don't techincally need the synchronized in the method
        // but I thought I would throw it in anyway. When using a synchronized block, we need to declare the object
        // that needs to be synchronized. In this case, because we are acting on clients, we need to make sure that
        // adding to it does not impact another thread, therefore we wait for the clients instance not to be used
        // by any other thread, then we lock it so nothing else can interfere with it, then we add to it and release it.
        // This makes sure that a client cannot be added while the list is being fetched and ensures if the read is
        // happening after this is called, the client will be added beforehand. This, most importantly, prevents the
        // list changing size when we are doing something important with it. This is where you run into most of the
        // Concurrent Exceptions
        synchronized (clients) {
            return clients.add(client);
        }
    }

    /**
     * This function returns the list of clients. It is marked as synchronised which is an indicator that it
     * will make sure that this function can only be called one at a time. If if is called twice at exactly the same
     * time, it will make one wait until the other is completed and make sure that its changes are echoed to the other
     * waiting thread. This returns a 'CopyOnWriteArraySet' rather than the actual clients list. This basically takes
     * a copy of the list and returns it. This allows the other thread to iterate through the list and everything it
     * needs to even if the actual list of clients changes. Otherwise, this would lead to issues such as if a client is
     * removed while we are iterating through it as then the list changes size and Java shits itself. This just protects
     * us from that.
     *
     * @return {@link CopyOnWriteArraySet} A copy of the lists of clients at the time the function is called
     */
    public synchronized static CopyOnWriteArraySet<Client> getClients() {
        synchronized (clients) {
            return new CopyOnWriteArraySet<>(clients);
        }
    }

    /**
     * This function removes a client to the list of clients. It is marked as synchronised which is an indicator that it
     * will make sure that this function can only be called one at a time. If if is called twice at exactly the same
     * time, it will make one wait until the other is completed and make sure that its changes are echoed to the other
     * waiting thread
     *
     * @param client {@link Client} The client to remove
     * @return boolean <code>true</code> if this set contained the specified element
     */
    public synchronized static boolean removeClient(Client client) {
        synchronized (clients) {
            return clients.remove(client);
        }
    }

    /**
     * This attempts to distribute the given message from the given client to the rest of the clients sent to the
     * server. It is synchronized for the same reasons as before
     * @param message
     * @param sender
     */
    public synchronized static void broadcastMessage(String message, Client sender) {
        // As we are using the clients list, we need to make sure that we are the only ones interacting with it so
        // therefore we need to synchronize with it.
        synchronized (clients) {
            // Then we construct the message we are sending. This is my choice but it contains the identifier we gave
            // the client when they connected, the message and the length of the message all together in the form:
            //  [<client identifier>]: <message>
            String toDistribute = (message.length() + 4 + sender.getUuid().toString().length()) + ":" + "[" + sender.getUuid().toString() + "]: " + message;
            // Then we fetch an iterator for the list. We could have technically used a for loop here and just iterate
            // through it as normal but this causes a lot of issues with what I plan to do. If you may need to remove
            // elements from a thing you are iterating through, you basically need to use an iterator in Java. This
            // allows you to iterate basically as normal but also remove elements when you need without issues. This is
            // because if a client has failed, we need to remove it from the server and this is when we will find out
            Iterator<Client> clientIterator = clients.iterator();
            // While we still have a client left to iterate over
            while (clientIterator.hasNext()) {
                // We fetch the next client available
                Client client = clientIterator.next();
                // Then we don't want to send the message a client sent back to themselves because they can still see
                // what they typed.
                if (!client.getUuid().equals(sender.getUuid())) {
                    // Then we try to send the message to the client. If everything is all nice and dandy, this will
                    // work perfectly, rainbows will shine and birds will sing. But if something is wrong, just as if a
                    // client has quit without sending a disconnect instruction, it will fail as the function has no
                    // way to actually write to the socket because its closed. It's trying to write to nothing. This is
                    // reported as an IOException with a message ending with 'socket write error'. Therefore, if this
                    // happens we just remove the client with a message and continue on so everything works like magic.
                    // Otherwise it is probably an actual error so we dump the stack trace.
                    try {
                        client.sendMessage(toDistribute);
                    } catch (IOException e) {
                        if (e.getMessage().endsWith("socket write error")) {
                            clientIterator.remove();
                            System.err.println("Client [" + client.getUuid().toString() + "] has forcefully disconnected");
                        } else {
                            System.err.println("Failed to send message to client [" + sender.getUuid().toString() + "]. Stack trace to follow");
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

}
