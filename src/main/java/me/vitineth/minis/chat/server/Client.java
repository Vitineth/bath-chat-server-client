package me.vitineth.minis.chat.server;

import java.io.IOException;
import java.net.Socket;
import java.util.UUID;

/**
 * File created by Ryan (vitineth).<br>
 * Created on 17/02/2018.
 *
 * @author Ryan (vitineth)
 * @since 17/02/2018
 */
public class Client implements Comparable{

    private Socket socket;
    private UUID uuid;

    public Client(Socket socket, UUID uuid) {
        this.socket = socket;
        this.uuid = uuid;
    }

    public Client(Socket socket) {
        this(socket, UUID.randomUUID());
    }

    public void sendMessage(String message) throws IOException{
        this.socket.getOutputStream().write(message.getBytes());
        this.socket.getOutputStream().flush();
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public int compareTo(Object o) {
        return o instanceof Client && ((Client)o).getSocket().equals(socket) && ((Client)o).getUuid().equals(uuid) ? 0 : -1;
    }
}
