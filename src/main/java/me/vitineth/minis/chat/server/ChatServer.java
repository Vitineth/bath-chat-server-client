package me.vitineth.minis.chat.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Scanner;

/**
 * File created by Ryan (vitineth).<br>
 * Created on 17/02/2018.
 *
 * @author Ryan (vitineth)
 * @since 17/02/2018
 */
public class ChatServer {

    public static void main(String[] args){
        // NOTE: Any points that relate to a part of the specification will be marked like this:
        //   <<Server, 1>> which relates to the server specification, point 1.

        // ====

        // This is the default port that the specification requires.
        int port = 14001;

        // If the user has provided actual arguments to the program we will need to check them all in case they have
        // provided a different port for the server to act on
        if (args.length > 0){
            for (int i = 0; i < args.length; i++) {
                if (args[i].equals("-csp")){
                    // If they have provided the port argument, we need to check that there is another parameter after
                    // it so that we can actually parse it and change the port. If not tell them they are stupid and
                    // exit
                    if (i == args.length - 1){
                        System.err.println("You must specify a valid port after the -csp flag");
                        return;
                    }else{
                        // As my computing teachers tend to say "users are idiots, if you ask them what 2 + 2 in,
                        // one user will give you '2', another will give you 'two' and one will just lick the keyboard"
                        // So as a result, we need to try casting the input they gave (which is a string) to an integer
                        // this will fail if it is anything but a set of numbers, however the function throws a runtime
                        // error which means you technically don't NEED to catch it but it will screw you if you don't.
                        try{
                            port = Integer.parseInt(args[i+1]);
                        }catch (NumberFormatException e){
                            System.err.println("You must provide a valid numerical port for the server after the -csp flag");
                            return;
                        }
                    }
                }
            }
        }

        try {
            // <<Server, 2>>
            // <<Server, 7>>
            // <<Server, 9>>
            // First, we set up the server socket on the port that is required
            ServerSocket socket = new ServerSocket(port);

            // Now, we have three threads in this program:
            // 1. Accept Thread:
            //      This thread handles accepting new clients. This is explained more in AcceptRunnable and why we need
            //      it but all this thread does is keep trying to accept new clients and adding them to the list of
            //      clients
            // 2. Message Thread:
            //      This thread handles messages from the clients. It will try to read from each client in turn and then
            //      dispatch the messages to the rest of the clients through the server storage
            // 3. Main Thread (this thread):
            //      After launching the two other threads, this thread just waits for the user to input 'exit' so that
            //      it can exit which is explained below

            AcceptRunnable acceptRunnable = new AcceptRunnable(socket);
            MessageRunnable messageRunnable = new MessageRunnable();

            // Runnables contain the code that can be executed by a thread but they do not actually run concurrently
            // unless they are wrapped in a thread which is what we do here.
            Thread acceptThread = new Thread(acceptRunnable, "Accept Runnable");
            Thread messageThread = new Thread(messageRunnable, "Message Runnable");

            // Now that we have threads wrapping the code of each of the runnables, we need to start them so they
            // actually run concurrently.
            messageThread.start();
            acceptThread.start();

            // <<Server, 6>>
            // Once the threads start, we need to start checking if the user enters 'exit' in order to stop the server.
            // The easiest way to read user inputs is to use the scanner class as we can just read by line.
            Scanner scanner = new Scanner(System.in);
            // This is basically a quick cheat to block until the user enters 'exit'. 'nextLine' is blocking meaning
            // that this thread will pause until the user enters anything. Then, if it is not exit, the while loop will
            // execute again which will cause it to block again. Only when they type 'exit' will the loop break and
            // we can move on to stopping the server.
            while (!scanner.nextLine().equalsIgnoreCase("exit"));

            // First we tell both of the runnables that they should stop at this point. This will kill messageRunnable
            // but not acceptRunnable but we explore that next.
            acceptRunnable.stop();
            messageRunnable.stop();

            // Second, we need to close the server socket. This must be done before we stop the threads. This was an
            // issue that I actually ran into myself. the 'accept' method in 'AcceptRunnable' is blocking so the thread
            // will stop until there is a new client. This means that we cannot exit it randomly in the normal way.
            // However, if we close the socket, the accept function will throw an error and break which means that we
            // can then stop the server. As we have stopped both of the runnables, as soon as this happens, the accept
            // thread will stop completely and the thread will nicely end.
            socket.close();

            // Finally, we 'join' the threads. This just waits until they end and then the program will finally end and
            // exit completely.
            acceptThread.join();
            messageThread.join();
        } catch (IOException e) {
            System.err.println("There was an error binding to the provided port. Stack trace to follow");
            e.printStackTrace();
        } catch (InterruptedException e) {
            System.err.println("There was an error waiting for the threads to terminate. Stack trace to follow");
            e.printStackTrace();
        }
    }
}
