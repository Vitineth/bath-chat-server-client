package me.vitineth.minis.chat.server;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * File created by Ryan (vitineth).<br>
 * Created on 17/02/2018.
 *
 * @author Ryan (vitineth)
 * @since 17/02/2018
 */
public class MessageRunnable implements Runnable {

    /**
     * Whether the current runnable is considered to be running
     */
    private boolean running;

    public MessageRunnable() {
        this.running = true;
    }

    /**
     * Attempts to stop the runnable.<br>
     * <b>Warning</b>: This does not actually terminate the server, if the loop is executing then it may drop out
     * but if accept is blocking, this will not terminate the runnable
     */
    public void stop() {
        running = false;
    }

    @Override
    public void run() {
        // We need to keep running while this thread is considered to be running.
        while (running) {
            // We take a copy of the list of clients from the server storage. The reason for this is explained more
            // in the getClients function and how it works.
            CopyOnWriteArraySet<Client> clients = ServerStorage.getClients();
            // Then, for each client in the clients we need to try and read a message from then
            for (Client client : clients) {
                try {
                    // First, get a reference to the clients Input Stream. This is only done so that I don't have to
                    // keep tying client.getSocket().getInputStream() because screw typing all that.
                    InputStream is = client.getSocket().getInputStream();
                    // Available returns an ESTIMATE of the number of bytes available to read in the stream. It is good
                    // for telling whether there is something available to read but SHOULD NOT be used as an indicator
                    // of the number of bytes to read. This is because it may not be complete or may be an overestimate.
                    // If it is an underestimate, you will only get part of a message and if it is an overestimate we
                    // will just keep trying to read and it will block up the thread.
                    if (is.available() > 0) {
                        // We need this to copy bytes from the input stream in to. It acts just like a standard
                        // output stream but allows us to convert it to a string straight away once we are done and
                        // just makes things easier
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        int read;
                        // This is a bit of a weird construct and a bit of a cheat. If the read function returns -1, it
                        // actually means that the input stream has been closed but it just provides us a quick method
                        // to keep reading until we break. In this loop, we keep individual bytes until we reach the
                        // first colon in the message. This is because I decided to use the message construct:
                        //  '<length of message>:<actual message>'
                        // This is because we need to define the protocol by which the client and the server will
                        // communicate. There is no good reliable way to naturally detect from the server that the
                        // client is done talking, therefore the client needs to provide a way for the server to know
                        // that they have done talking. My decision was for the client to tell the server exactly how
                        // many bytes it is sending. This also makes the next few lines really, really easy.
                        // Before we reach the first colon, we need to store each of the bytes we receive as they will
                        // indicate the number of bytes. Therefore, we write them to out byte array output stream.
                        // There are a couple of ways to do this but this is one that just comes naturally to me.
                        while ((read = is.read()) != -1){
                            if ((char) read == ':') break;
                            baos.write(read);
                        }

                        // By the time we reach this point, we know that (in theory), the ByteArrayOutputStream contains
                        // just the number of bytes to read, therefore we convert the bytes to a string (I explicitly
                        // state that it will be in UTF_8 but this is not actually required). Then we define a buffer
                        // of that length and tell the input stream to read that many bytes from the stream into the
                        // buffer and, again in theory, it should read the entire message from it.
                        String numberOfBytesString = baos.toString(StandardCharsets.UTF_8.name());
                        int numberOfBytesToRead = Integer.parseInt(numberOfBytesString);
                        byte[] buffer = new byte[numberOfBytesToRead];
                        is.read(buffer);

                        // Then we convert the byte array to a string, again explicitly as UTF_8 and as long as it does
                        // not equal 'exit', we send the message on to the rest of the clients. If it does equal 'exit',
                        // we know that this client is about to disconnect from the server so we remove the client.
                        String message = new String(buffer, StandardCharsets.UTF_8);
                        if (message.equalsIgnoreCase("exit")){
                            ServerStorage.removeClient(client);
                            System.err.println("Client [" + client.getUuid().toString() + "] has disconnected");
                        }else {
                            ServerStorage.broadcastMessage(message, client);
                        }
                    }
                } catch (IOException e) {
                    System.err.println("Failed to read from client [" + client.getUuid().toString() + "]. Stack trace to follow");
                    e.printStackTrace();
                }
            }
        }
    }
}
