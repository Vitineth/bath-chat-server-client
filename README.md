# README #

It has no dependencies but it still used Maven. This just means the project structure is a little bit nicer. The server launches through ChatServer and the client launches through ChatClient. However, it doesn't meet points 8 and 7 on server and clients lists respectively. Launching the server and the client from command line is a bit annoying.

If you want to demonstrate that it works, download the JAR file from [here](https://bitbucket.org/Vitineth/bath-chat-server-client/downloads/chat.jar) and run the following commands:

|To Do|Command|
|---|---|
|Launch the **Server**|`java -cp chat.jar me.vitineth.minis.chat.server.ChatServer`|
|Launch the **Client**|`java -cp chat.jar me.vitineth.minis.chat.client.ChatClient`|